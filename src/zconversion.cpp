// Copyright (C) 2015-2020 Leonardo Silvestri
//
// This file is part of ztsdb.
//
// ztsdb is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ztsdb is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ztsdb.  If not, see <http://www.gnu.org/licenses/>.

// For the algo's we use a modified version of Howard Hinnant's code
// in "date.h" (http://howardhinnant.github.io/date_v2.html with the
// algos further explained here:
// http://howardhinnant.github.io/date_algorithms.html). The license
// for this modified code is:

// The MIT License (MIT)
// 
// Copyright (c) 2015 Howard Hinnant
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#include <ztsdb/globals.hpp>
#include "rztsdb/zconversion.hpp"
#include "globals.hpp"          // from 'nanotime'
#include "utilities.hpp"        // from 'nanotime'
#include "interval.hpp"         // from 'nanotime'



// This file handles the conversion from ztsdb to R.


static Rcpp::List convertDimnames(const std::vector<std::unique_ptr<arr::Dname>>& dnames) {
  Rcpp::List l;
  for (auto& n : dnames) {
    Rcpp::CharacterVector cv;
    for (auto& e : n->names) {
      cv.push_back(e);
    }
    l.push_back(cv);
  }
  return l;
}


template <int RTYPE, typename T>
static Rcpp::Vector<RTYPE> convertArray(const arr::Array<T>& a, bool force_one_dim=false) {
  // Note: a first version of these functions used
  // 'Rcpp::Vector::push_back', but that proved to be incredibly slow,
  // so instead we allocate the vector upfront and then subassign.

  Rcpp::Vector<RTYPE> v(a.size()); // unfortunately we have initialization here...
                                   // we would be faster using directly the SEXP
                                   // allocate/populate SEXP, then construct v with it
  uint64_t offset = 0;
  for (const auto& av : a.v) {
    memcpy(&v[offset], &(*av)[0], sizeof(T) * a.nrows());
    offset += a.nrows();
  }
  if (a.getdim().size() > 1) {
    if (force_one_dim) {
      Rcpp::warning("cannot preserve array shape for type in R: forcing ztsdb array to vector");
    }
    else {
      v.attr("dim") = Rcpp::IntegerVector(a.getdim().begin(), a.getdim().end());
    }
  }
  if (a.hasNames()) {
    if (a.getdim().size() == 1 || force_one_dim) {
      v.attr("names") = convertDimnames(a.names)[0];
    }
    else {
      v.attr("dimnames") = convertDimnames(a.names);
    }
  }
  return v;
}


static Rcpp::Vector<LGLSXP> convertBoolArray(const arr::Array<bool>& a) {
  Rcpp::Vector<LGLSXP> v(a.size()); // unfortunately we have initialization here...
                              // we would be faster using directly the SEXP
  uint64_t offset = 0;
  for (const auto& av : a.v) {
    for (arr::idx_type j=0; j<a.nrows(); ++j) {
      v[offset + j] = (*av)[j];
    }
    offset += a.nrows();
  }
  if (a.getdim().size() > 1) {
    v.attr("dim") = Rcpp::IntegerVector(a.getdim().begin(), a.getdim().end());
  }
  if (a.hasNames()) {
    if (a.getdim().size() == 1) {
      v.attr("names") = convertDimnames(a.names)[0];
    }
    else {
      v.attr("dimnames") = convertDimnames(a.names);
    }
  }
  return v;
}


static Rcpp::Vector<STRSXP> convertStringArray(const arr::Array<arr::zstring>& a) {
  Rcpp::Vector<STRSXP> v(a.size()); // unfortunately we have initialization here...
                                    // we would be faster using directly the SEXP
  uint64_t offset = 0;
  for (const auto& av : a.v) {
    for (arr::idx_type j=0; j<a.nrows(); ++j) {
      v[offset + j] = std::string((*av)[j]);
    }
    offset += a.nrows();
  }
  if (a.getdim().size() > 1) {
    v.attr("dim") = Rcpp::IntegerVector(a.getdim().begin(), a.getdim().end());
  }
  if (a.hasNames()) {
    if (a.getdim().size() == 1) {
      v.attr("names") = convertDimnames(a.names)[0];
    }
    else {
      v.attr("dimnames") = convertDimnames(a.names);
    }
  }
  return v;
}


static Rcpp::ComplexVector convertIntervalArray(const arr::Array<tz::interval>& a) {
  Rcpp::ComplexVector v(a.size());
  uint64_t offset = 0;  
  for (const auto& av : a.v) {
    for (arr::idx_type j=0; j<a.nrows(); ++j) {
      // the conversion is not straightforward: in R `interval` is coded with 63-bit start and end...
      tz::interval ztsdb_ival = (*av)[j];
      interval nanotime_ival = interval(ztsdb_ival.s, ztsdb_ival.e, ztsdb_ival.sopen, ztsdb_ival.eopen);
      memcpy(&v[offset + j], &nanotime_ival, sizeof(Rcomplex));
    }
    offset += a.nrows();
  }
  if (a.getdim().size() > 1) {
    Rcpp::warning("cannot preserve array shape for `nanoival` in R: forcing ztsdb array to vector");
  }
  if (a.hasNames()) {
    v.attr("names") = convertDimnames(a.names)[0]; // only preserve names in first dim: that's the best we can do
  }
  return v;
}


static Rcpp::ComplexVector convertPeriodArray(const arr::Array<tz::period>& a) {
  Rcpp::ComplexVector v(a.size());
  uint64_t offset = 0;
  for (const auto& av : a.v) {
    memcpy(&v[offset], &(*av)[0], sizeof(tz::period) * a.nrows());
    offset += a.nrows();
  }
  if (a.getdim().size() > 1) {
    Rcpp::warning("cannot preserve array shape for `nanoperiod` in R: forcing ztsdb array to vector");
  }
  if (a.hasNames()) {
    v.attr("names") = convertDimnames(a.names)[0]; // only preserve names in first dim: that's the best we can do
  }
  return v;
}


static std::string buildName(const vector<unique_ptr<arr::Dname>>& s, std::vector<size_t>& counter) {
  std::string res;
  for (size_t i=1; i<counter.size(); ++i) { // 1 because we don't take row names
    // get the string representation
    const arr::Dname& name = *s[i];
    if (name.size()) {
      res += (res.size() ? "_" : "") + name[counter[i]];
    }
    else {
      res += (res.size() ? "_" : "V") + std::to_string(counter[i]);
    }
  }
  return res;
}

static void convertNamesForDt(const vector<unique_ptr<Dname>>& s,
                              const Vector<size_t>& dim,
                              size_t ncols,
                              Rcpp::StringVector& names) {
  // build a counter for each dimension:
  std::vector<size_t> counter(dim.size());
  
  for (size_t n = 0; n<ncols; ++n) {
    names[n + 1] = buildName(s, counter); // + 1 because first column is "index"
    // figure out the counter to increment:
    for (size_t i=1; i<dim.size(); ++i) { // 1 because we don't take row names
      if (++counter[i] < dim[i]) {
        break;
      }
      else {
        counter[i] = 0;         // and continue the loop as we have a carry
      }
    }
  }

  // for (int i=0; i<names.size(); ++i) {
  //   Rcpp::Rcout << names[i] << std::endl;
  // }
}


static Rcpp::List convertZts(const arr::zts& z) {
  // convert the index:
  const auto zts_idx = z.getIndexPtr()->getcol(0);
  Rcpp::NumericVector dt_idx(zts_idx.size());
  memcpy(&dt_idx[0], &zts_idx[0], zts_idx.size() * sizeof(Global::dtime));
  assignS4("nanotime", dt_idx, "integer64");

  const auto a = z.getArray();
  Rcpp::List dt(a.ncols() + 1); // + 1 for the index
  dt[0] = dt_idx;

  // if dims are > 2 we flatten and warn:
  if (a.getdim().size() > 2) {
    Rcpp::warning("cannot preserve 'zts' dimensions, reshaping to 2D");
  }

  // convert the double array:
  int col = 1;
  for (const auto& av : a.v) {
    Rcpp::NumericVector v_col(a.nrows());
    memcpy(&v_col[0], &(*av)[0], sizeof(double) * a.nrows());
    dt[col++] = v_col;
  }

  // handle column names:
  Rcpp::StringVector strv(a.ncols() + 1);
  strv[0] = "index";
  if (a.hasNames()) {
    if (a.getdim().size() <= 2) {
      for (size_t i=0; i<a.names[1]->size(); ++i) {
        strv[i+1] = std::string(a.names[1]->names[i]);
      }
    }
    else {
      convertNamesForDt(a.names, a.getdim(), a.ncols(), strv);
    }
  } else {
    if (a.getdim().size() == 1) {
      strv[1] = "V1";
    }
    else {
      // names with numbers "V_" as `data.table` sort of does:
      convertNamesForDt(a.names, a.getdim(), a.ncols(), strv);
    }
  }
  dt.attr("names") = strv;

  dt.attr("class") = Rcpp::CharacterVector::create("data.table", "data.frame");
  return dt;
}


static Rcpp::List convertList(const arr::Array<val::Value>& a) {
  Rcpp::List l(a.size());
  for (size_t j=0; j<a.size(); ++j) {
    l[j] = valueToSEXP(a[j]);
  }
  if (a.hasNames()) {
    auto names = Rcpp::CharacterVector(a.size());
    for (size_t j=0; j<a.size(); ++j) {
      names[j] = a.getNames(0)[j];
    }
    l.names() = names;
  }
  return l;
}


SEXP valueToSEXP(const val::Value& v) {
  switch (v.which()) {
  case val::vt_zts: {
    const auto& ts = get<val::SpZts>(v);
    Rcpp::List v = convertZts(*ts);
    return Rcpp::wrap(v);
  }
  case val::vt_double: {
    const auto& a = get<val::SpVAD>(v);
    Rcpp::NumericVector v = convertArray<REALSXP, double>(*a);
    return Rcpp::wrap(v);
  }
  case val::vt_bool: {
    const auto& a = get<val::SpVAB>(v);
    Rcpp::LogicalVector v = convertBoolArray(*a);
    return Rcpp::wrap(v);
  }
  case val::vt_time: {
    const auto& a = get<val::SpVADT>(v);
    Rcpp::NumericVector v = convertArray<REALSXP, Global::dtime>(*a, true); // true: force one dimension
    return Rcpp::wrap(assignS4("nanotime", v, "integer64"));
  }
  case val::vt_duration: {
    const auto& a = get<val::SpVADUR>(v);
    Rcpp::NumericVector v = convertArray<REALSXP, Global::duration>(*a, true); // true: force one dimension
    return Rcpp::wrap(assignS4("nanoduration", v, "integer64"));
  }
  case val::vt_interval: {
    const auto& a = get<val::SpVAIVL>(v);
    Rcpp::ComplexVector v = convertIntervalArray(*a);
    return Rcpp::wrap(assignS4("nanoival", v));
  }
  case val::vt_period: {
    const auto& a = get<val::SpVAPRD>(v);
    Rcpp::ComplexVector v = convertPeriodArray(*a);
    return Rcpp::wrap(assignS4("nanoperiod", v));    
  }
  case val::vt_string: {
    const auto& a = get<val::SpVAS>(v);
    Rcpp::CharacterVector v = convertStringArray(*a);
    return Rcpp::wrap(v);
  }
  case val::vt_list: {
    const auto& a = get<val::SpVList>(v);
    Rcpp::List l = convertList(a->a);
    return Rcpp::wrap(l);
  }
  case val::vt_null: {
    return Rcpp::wrap(R_NilValue);      
  }
  case val::vt_error: {
    const auto& e = get<val::VError>(v);
    throw std::range_error(e.what);
  }
  default:
    throw std::range_error("unknown return value"s + std::to_string(v.which()));
  }
}


