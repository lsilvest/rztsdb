## test the escapement of each supported type; this tests the round
## trip from R to ztsdb and back.

## systematically call each type we support with size 0, scalar, and vector/matrix/array (where supported):

## double:
## ------
test_double_0 <- function() {
  checkEquals(numeric(), c1 ? ++numeric())
}
test_double_scalar <- function() {
  checkEquals(1, c1 ? ++1)
}
test_double_scalar_multiple_plus <- function() {
  checkEquals(6, c1 ? (1 + 2 + 3))
}
test_double_scalar_multiple_plus <- function() {
  a <- 1; b <- 2; c <- 3
  checkEquals(6, c1 ? (++a + ++b + ++c))
}
test_double_matrix <- function() {
  m <- matrix(1:9, 3, 3, dimnames=list(c("1","2","3"), c("a","b","c")))
  checkEquals(m, c1 ? ++m)
}
test_double_array <- function() {
  a <- array(1:27, c(3, 3, 3), dimnames=list(c("1","2","3"), c("a","b","c"), c("i","ii","iii")))
  checkEquals(a, c1 ? ++a)
}


## bool:
## ----
test_bool_0 <- function() {
  checkEquals(logical(), c1 ? ++logical())
}
test_bool_scalar <- function() {
  checkEquals(TRUE, c1 ? ++TRUE)
}
test_bool_vector <- function() {
  checkEquals(c(a=TRUE,b=FALSE), c1 ? ++c(a=TRUE,b=FALSE))
}
test_bool_matrix <- function() {
  m <- matrix(rep(c(TRUE, FALSE), 9), 3, 3, dimnames=list(c("1","2","3"), c("a","b","c")))
  checkEquals(m, c1 ? ++m)
}
test_bool_array <- function() {
  a <- array(rep(c(TRUE,FALSE,TRUE), 9), c(3, 3, 3), dimnames=list(c("1","2","3"), c("a","b","c"), c("i","ii","iii")))
  checkEquals(a, c1 ? ++a)
}


## nanotime:
## --------
test_time_0 <- function() {
  p <- as.nanotime()
  checkEquals(p, c1 ? ++p)
}
test_time_scalar <- function() {
  p <- as.nanotime("1970-01-01 UTC")
  checkEquals(p, c1 ? ++p)
}
test_time_vector <- function() {
  p <- as.nanotime("1970-01-01 UTC") + 1:10
  checkEquals(p, c1 ? ++p)
}


## duration:
## --------
test_duration_0 <- function() {
  p <- as.nanoduration()
  checkEquals(p, c1 ? ++p)
}
test_duration_scalar <- function() {
  p <- as.nanoduration(1)
  checkEquals(p, c1 ? ++p)
}
test_duration_vector <- function() {
  p <- as.nanoduration(1:10)
  checkEquals(p, c1 ? ++p)
}


## interval:
## --------
test_interval_0 <- function() {
  p <- as.nanoival()
  checkEquals(p, c1 ? ++p)
}
test_interval_scalar <- function() {
  p <- nanoival(as.nanotime(1), as.nanotime(2))
  checkEquals(p, c1 ? ++p)
}
test_interval_vector <- function() {
  p <- nanoival(as.nanotime(1:10), as.nanotime(2:11))
  checkEquals(p, c1 ? ++p)
}


## character:
## ---------
test_string_0 <- function() {
  checkEquals(character(), c1 ? ++character())
}
test_string_scalar <- function() {
  checkEquals("how soon is now?", c1 ? ++"how soon is now?")
}
test_string_vector <- function() {
  s <- matrix(as.character(1:10), 5, 2)
  checkEquals(s, c1 ? ++s)
}
test_string_array <- function() {
  s <- matrix(as.character(1:27), c(3,3,3))
  checkEquals(s, c1 ? ++s)
}


## zts:
## ---
test_zts_0 <- function() {                # 0 rows
  x <- data.table(index = as.nanotime(), data=numeric())
  checkEquals(x, c1 ? ++x)
}
test_zts <- function() {
  x <- data.table(index = as.nanotime("1970-01-01 UTC") + 1:9, data=1:9)
  checkEquals(x, c1 ? ++x)
}
test_zts_unnamed <- function() {
  x <- data.table(index = as.nanotime("1970-01-01 UTC") + 1:9, 1:9)
  checkEquals(x, c1 ? ++x)
}


## note: list is not yet round-trip
