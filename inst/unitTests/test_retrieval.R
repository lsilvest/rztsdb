## test the retrieval of every type we support

## scalars; 
test_double_scalar <- function() {
  checkEquals(1, c1 ? 1)
}
test_bool_scalar <- function() {
  checkEquals(TRUE, c1 ? TRUE)
}
test_time_scalar <- function() {
  checkEquals(as.nanotime("1970-01-01 UTC"), c1 ? as.nanotime("1970-01-01 00:00:00 UTC"))
}
test_string_scalar <- function() {
  checkEquals("how soon is now?", c1 ? "how soon is now?")
}
test_duration_scalar <- function() {
  checkEquals(as.nanoduration(10), c1 ? as.nanoduration(10))
}
test_interval_scalar <- function() {
  checkEquals(as.nanoduration(10), c1 ? as.nanoduration(10))
}

## vectors; 
test_double_vector <- function() {
  checkEquals(c(a=1,b=2), c1 ? c(a=1,b=2))
}
test_bool_vector <- function() {
  checkEquals(c(a=TRUE,b=FALSE), c1 ? c(a=TRUE,b=FALSE))
}
test_string_vector <- function() {
  checkEquals(c(a="how soon", b="is", c="now?"), c1 ? c(a="how soon", b="is", c="now?"))
}


## arrays; these have equality
test_double <- function() {
  checkEquals(matrix(1:9, 3, 3, dimnames=list(c("1","2","3"), c("a","b","c"))),
              c1 ? matrix(1:9, 3, 3, dimnames=list(c("1","2","3"), c("a","b","c"))))
}
test_bool <- function() {
  checkEquals(matrix(c(T,T,F,F), 2, 2), c1 ? matrix(c(T,T,F,F), 2, 2))
}
test_time <- function() {
  checkEquals(nanotime("1970-01-01 UTC") + 1:10*as.nanoduration(1e9),
              c1 ? as.nanotime("1970-01-01 00:00:00 UTC") + 1:10*as.nanoduration(1e9))
}
test_string <- function() {
  checkEquals(matrix(as.character(1:10), 5, 2), c1 ? matrix(as.character(1:10), 5, 2))
}
# zts may have tzone difference, so here again don't use checkEquals
test_zts <- function() {
  x <- data.table(index=as.nanotime("1970-01-01 UTC") + 1:9, V1=1:9)
  z <- (c1 ? zts(as.nanotime("1970-01-01 00:00:00 UTC") + 1:9*as.nanoduration(1e9), 1:9))
  checkEquals(x, z)
}

## list
test_list_double <- function() {
  checkEquals(list(1,2,3), c1 ? list(1,2,3))
}
test_list_nested <- function() {
  checkEquals(list(1, list(1,"2",TRUE), 3), c1 ? list(1, list(1,"2",TRUE), 3))
}
test_list_double_named <- function() {
  checkEquals(list(a=1,b=2,c=3), c1 ? list(a=1,b=2,c=3))
}
test_list_double_partially_named <- function() {
  checkEquals(list(a=1,2,c=3), c1 ? list(a=1,2,c=3))
}
test_list_nested_partially_named <- function() {
  checkEquals(list(1, list(a=1,"2",TRUE), c=3), c1 ? list(1, list(a=1,"2",TRUE), c=3))
}
  
